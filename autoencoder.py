import numpy as np
import keras
import matplotlib.pyplot as plt
import math
import sys
import getopt
import random
from keras import layers
from keras.models import load_model

from src.input import get_image_data, get_positive_number
from src.autoencoder_maker import make_autoencoder, train_autoencoder

# init
args = sys.argv[1:]
optlist, args = getopt.getopt(args,"d:")
(_,train_image_file) = optlist[0]

# get train data
train_images, num_images, rows, cols = get_image_data(train_image_file)
train_images = train_images.astype('float32')/255.0

#making the data collections for the final summary
sum_names = ()
sum_loss = []
sum_filter_size = []
sum_epochs = []
sum_batch_size = []
count = 0

new_autoencoder = True
while new_autoencoder:
	# input
	number_of_layers = get_positive_number('Input Number of Layers:')
	filter_size = get_positive_number('Input Filter Size:')
	filter_size = (filter_size,filter_size)
	filter_nums = []
	for x in range(number_of_layers):
		filter_nums.append(get_positive_number('Input Filters for Layer '+str(x+1)+':'))
	epochs = get_positive_number('Input Epochs:')
	batch_size = get_positive_number('Input Batch Size:')

	# create & train autoencoder
	autoencoder, train_history = train_autoencoder(make_autoencoder(rows, cols, number_of_layers, filter_size, filter_nums), train_images, epochs, batch_size)

	#data collection for the final summary
	tempname = 'test_'+str(count)
	sum_names = sum_names + (tempname,)
	count = count + 1
	sum_loss.append(train_history.history['loss'][-1])
	sum_filter_size.append(filter_size[0])
	sum_epochs.append(epochs)
	sum_batch_size.append(batch_size)

	flag = True
	prompt = '\nSelect an option:\n0:Exit\n1:Repeat the training proccess with new variables\n2:Print training history plots\n3:Save current autoencoder to file\n\n-:'
	while flag:
		num = int(input(prompt))
		if num == 0:
			# exit
			new_autoencoder = False
			flag = False
		elif num == 1:
			# new autoencoder
			flag = False
		elif num == 2:
			# show history plots
			plt.plot(train_history.history['loss'])
			plt.plot(train_history.history['val_loss'])
			plt.ylabel('loss')
			plt.xlabel('epochs')
			plt.legend(['train', 'validation'], loc='upper right')
			plt.show()

			#making data graph
			fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)
			ax1.bar(sum_names, sum_loss, color='red' )
			ax1.set_ylabel('Final Loss')
			ax2.bar(sum_names, sum_filter_size, color='orange')
			ax2.set_ylabel('Filter Size')
			ax3.bar(sum_names, sum_epochs )
			ax3.set_ylabel('Epochs')
			ax4.bar(sum_names, sum_batch_size, color='green')
			ax4.set_ylabel('Batch Size')
			plt.show()

		elif num == 3:
			# save autoencoder
			autoencoder_file = input('Autoencoder file:')
			autoencoder.save(autoencoder_file)

			# display example images

			n = 10
			plt.figure(figsize=(20, 4))
			plt.subplot(2, n, 1)
			plt.text(0, -6, "Original Images", va = 'top', fontsize=16, color = 'k')
			plt.subplot(2, n, n+1)
			plt.text(0, -6, "Recreated Images", va = 'top', fontsize=16, color = 'k')

			plt.gray()
			for i in range(n):
				# display original
				r = random.randrange(num_images)
				plt.subplot(2, n, i+1)
				plt.imshow(train_images[r].reshape(28, 28))
				plt.axis('off')

				# display reconstruction
				decoded_image = autoencoder.predict(train_images[r:r+1])
				plt.subplot(2, n, i+n+1)
				plt.imshow(decoded_image[0].reshape(28, 28))
				plt.axis('off')
			plt.show()
		else:
		    print('Please input a valid option.')
