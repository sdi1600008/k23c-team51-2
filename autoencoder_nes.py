import numpy as np
import keras
import matplotlib.pyplot as plt
import math
import sys
import getopt
import random
from keras import layers
from keras.models import load_model

from src.input import get_image_data, get_positive_number
from src.autoencoder_maker import make_autoencoder, train_autoencoder


def sort_and_rank(pop_loss,filename,mutation_chance,gen):
	for i in range(0,len(pop_loss)):
		for j in range(0,len(pop_loss)-i-1):
			if(random.randint(1,100)>mutation_chance):
				if(pop_loss[j][0] > pop_loss[j+1][0]):
					temp = pop_loss[j]
					pop_loss[j] = pop_loss[j+1]
					pop_loss[j+1] = temp

	output = pop_loss.copy()

	for i in range(0,len(pop_loss)):
		for j in range(0,len(pop_loss)-i-1):
			if(pop_loss[j][0] > pop_loss[j+1][0]):
				temp = pop_loss[j]
				pop_loss[j] = pop_loss[j+1]
				pop_loss[j+1] = temp

	file=open(filename,"w")
	for i in range(0,len(pop_loss)):
		line = 'Rank '+str(i+1) + ': gen'+str(gen)+'_'+str(pop_loss[i][1])
		file.write("%s\n"%line)
	file.close()
	return output

def reproduction(parent0,parent1,mutation_chance):
	rand=random.randint(0,1)
	mutation= random.randint(-1,1) * (random.randint(1,100)<=mutation_chance)
	if rand:
		output = parent1 + mutation
	else:
		output = parent0 + mutation
	if output > 0:
		return output
	else:
		return 1

args = sys.argv[1:]
optlist, args = getopt.getopt(args,"d:")
(_,train_image_file) = optlist[0]

gen_num = get_positive_number('Input number of Generations:')
#gen_num = 100
pop_num = get_positive_number('Input number of population:')
#pop_num = 24
#set random values for the first generation
pop = ()
pop_loss = []
for i in range(pop_num):
	number_of_layers = random.randint(2, 5)
	#number_of_layers = 3
	temp_tup = (number_of_layers,)
	filter_nums = []
	for j in range(number_of_layers):
		filter_nums.append(random.randint(8,64))

	temp_tup = temp_tup + (filter_nums,)
	#epochs = random.randint(5,50)
	epochs = random.randint(2,5)
	temp_tup = temp_tup + (epochs,)
	batch_size = random.randint(50,200)
	temp_tup = temp_tup + (batch_size,)
	pop = pop + (temp_tup,)
	del temp_tup;


#making the data collections for the final summary
sum_names = ()
sum_loss = []
sum_filter_size = []
sum_epochs = []
sum_batch_size = []
count = 0
path = './nes_data/'
for i in range(gen_num):
	print("Gen:",i)
	filename = path + 'reports/gen'+str(i)+'_report.txt'
	file=open(filename,"w")
	for j in range(pop_num):
		number_of_layers = pop[j][0]
		#number_of_layers = 3
		filter_size = (3,3)
		filter_nums = pop[j][1]
		epochs = pop[j][2]
		batch_size = pop[j][3]

		# get train data
		train_images, num_images, rows, cols = get_image_data(train_image_file)
		train_images = train_images.astype('float32')/255.0

		# create & train autoencoder
		autoencoder, train_history = train_autoencoder(make_autoencoder(rows, cols, number_of_layers, filter_size, filter_nums), train_images, epochs, batch_size)
		pop_loss.append((train_history.history['loss'][-1] , j))
		#add it to the vault
		tempname = 'test_'+str(count)
		sum_names = sum_names + (tempname,)
		count = count + 1
		sum_loss.append(train_history.history['loss'][-1])
		sum_filter_size.append(filter_size[0])
		sum_epochs.append(epochs)
		sum_batch_size.append(batch_size)
		#save the report
		line = "ID: gen"+str(i)+"_"+str(j)+" number of layers:"+str(pop[j][0])+" filter nums:"+str(pop[j][1])+" epochs:"+str(pop[j][2])+" batch size:"+str(pop[j][3])+"---> loss:"+str(train_history.history['loss'][-1])
		file.write("%s\n"%line)
		#save the neural network
		autoencoder.save(path+'nn/gen'+str(i)+'_'+str(j)+'.h5')
		#create and save the plot images
		decoded_imgs = autoencoder.predict(train_images)

		n = 10
		plt.figure(figsize=(20, 4))
		for z in range(1, n + 1):
			# Display original
			r = random.randrange(num_images)
			ax = plt.subplot(2, n, z)
			plt.imshow(train_images[r].reshape(28, 28))
			plt.gray()
			ax.get_xaxis().set_visible(False)
			ax.get_yaxis().set_visible(False)

			# Display reconstruction
			ax = plt.subplot(2, n, z + n)
			plt.imshow(decoded_imgs[r].reshape(28, 28))
			plt.gray()
			ax.get_xaxis().set_visible(False)
			ax.get_yaxis().set_visible(False)
		filename = path+'results/gen'+str(i)+'_'+str(j)+'.png'
		plt.savefig(filename)




	#gen finished doing work prepare new gen
	file.close()
	filename = path+'ranking/gen'+str(i)+'_ranking.txt'
	#5% chance for suboptimal parameters to survive
	sort_and_rank(pop_loss,filename,5,i)



	#making new gen
	parenta_index = 0
	parentb_index = 1
	index_flag = 0
	mutation = 10
	filename = path+'parents/gen'+str(i+1)+'_parents.txt'
	file=open(filename,"w")
	line = 'gen'+str(i)+'_'+str(pop_loss[parenta_index][1])+'+gen'+str(i)+'_'+str(pop_loss[parentb_index][1])+':'
	file.write("%s\n"%line)

	new_gen = ()
	for j in range(pop_num):
		parenta = pop[ pop_loss[parenta_index][1] ][0]
		parentb = pop[ pop_loss[parentb_index][1] ][0]
		new_number_of_layers = reproduction(parenta,parentb,mutation)
		new_gen_member = (new_number_of_layers,)
		new_filter_nums = []
		if pop_loss[parenta_index][1] < pop_loss[parentb_index][1]:
			min_number_of_layers = pop_loss[parenta_index][1]
			max_number_of_layers =pop_loss[parentb_index][1]
			max_parent_index = parentb_index
		else:
			min_number_of_layers = pop_loss[parentb_index][1]
			max_number_of_layers =pop_loss[parenta_index][1]
			max_parent_index = parenta_index
		for z in range(new_number_of_layers):
			if z < min_number_of_layers:
				parenta = pop[ pop_loss[parenta_index][1] ][1][z]
				parentb = pop[ pop_loss[parentb_index][1] ][1][z]
				new_filter_nums.append(reproduction(parenta,parentb,mutation))
			elif z < max_number_of_layers:
				new_filter_nums.append(pop[ pop_loss[max_parent_index][1] ][1][z])
			else:
				new_filter_nums.append(random.randint(8,64))
		parenta = pop[ pop_loss[parenta_index][1] ][2]
		parentb = pop[ pop_loss[parentb_index][1] ][2]
		new_gen_member = new_gen_member + (new_filter_nums,)
		new_epochs = reproduction(parenta,parentb,mutation)
		new_gen_member = new_gen_member + (new_epochs,)
		parenta = pop[ pop_loss[parenta_index][1] ][3]
		parentb = pop[ pop_loss[parentb_index][1] ][3]
		new_batch_size = reproduction(parenta,parentb,mutation)
		new_gen_member = new_gen_member + (new_batch_size,)
		new_gen = new_gen + (new_gen_member,)
		del new_gen_member;

		line='gen'+str(i+1)+'_'+str(j)
		file.write("%s\n"%line)

		if index_flag % 4 == 3:
			parenta_index = parenta_index + 2
			parentb_index = parentb_index + 2
			line = 'gen'+str(i)+'_'+str(pop_loss[parenta_index][1])+'+gen'+str(i)+'_'+str(pop_loss[parentb_index][1])+':'
			file.write("%s\n"%line)

		index_flag = index_flag + 1

	file.close()
	del pop;
	pop = new_gen
	del new_gen;
	del pop_loss;
	pop_loss = []




#making data graph
fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)
ax1.bar(sum_names, sum_loss, color='red' )
ax1.set_ylabel('Loss')
ax2.bar(sum_names, sum_filter_size, color='orange')
ax2.set_ylabel('Filter Size')
ax3.bar(sum_names, sum_epochs )
ax3.set_ylabel('Epochs')
ax4.bar(sum_names, sum_batch_size, color='green')
ax4.set_ylabel('Batch Size')

for i in plt.get_fignums():
     if i != fig.number:
         plt.close(i)

plt.show()
