import numpy as np
import keras
import matplotlib.pyplot as plt
import math
import sys
import getopt
import random
from keras.models import load_model

from src.input import get_image_data

args = sys.argv[1:]
optlist, args = getopt.getopt(args,"m:")
(_,autoencoder) = optlist[0]

test_images, num_images, rows, cols = get_image_data("./data/t10k-images.idx3-ubyte")
#test_images, num_images, rows, cols = get_image_data("./data/train-images.idx3-ubyte")

autoencoder = load_model(autoencoder)
autoencoder.summary()

test_images = test_images.astype('float32') / 255.
test_images = np.reshape(test_images, (len(test_images), 28, 28, 1))

decoded_imgs = autoencoder.predict(test_images)

n = 10
plt.figure(figsize=(2*n, 4))
plt.subplot(2, n, n/2)
plt.text(0, -6, "Original Images", va = 'top', fontsize=16, color = 'k')
plt.subplot(2, n, 3*n/2)
plt.text(0, -6, "Recreated Images", va = 'top', fontsize=16, color = 'k')

plt.gray()
for i in range(1, n + 1):
	# display original
	r = random.randrange(num_images)
	plt.subplot(2, n, i)
	plt.imshow(test_images[r].reshape(28, 28))
	plt.axis('off')

	# display reconstruction
	decoded_image = autoencoder.predict(test_images[r:r+1])
	plt.subplot(2, n, i + n)
	plt.imshow(decoded_image[0].reshape(28, 28))
	plt.axis('off')
plt.show()
