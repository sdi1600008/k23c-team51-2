import numpy as np
import keras
from keras import layers
from sklearn.model_selection import train_test_split

def encoder(input_img, input_shape, number_of_layers, filter_size, filter_nums):
	if number_of_layers < 1:
		print("Need at least 1 layer!")
		return

	if number_of_layers < 3:
		encoded = layers.Conv2D(filter_nums[0], filter_size, activation='relu', padding='same', input_shape=input_shape)(input_img)
		encoded = layers.BatchNormalization()(encoded)
		encoded =  layers.MaxPooling2D(pool_size=(2, 2), padding='same')(encoded)
		for i in range(1, number_of_layers):
			encoded = layers.Conv2D(filter_nums[i], filter_size, activation='relu', padding='same')(encoded)
			encoded = layers.BatchNormalization()(encoded)
			encoded = layers.MaxPooling2D(pool_size=(2, 2), padding='same')(encoded)

	else:
		if filter_size == 3:
			encoded = layers.Conv2D(filter_nums[0], filter_size, activation='relu', padding='same', input_shape=input_shape)(input_img)
			encoded = layers.BatchNormalization()(encoded)
			encoded =  layers.MaxPooling2D(pool_size=(2, 2), padding='same')(encoded)
			for i in range(1, number_of_layers):
				encoded = layers.Conv2D(filter_nums[i], filter_size, activation='relu', padding='same')(encoded)
				encoded = layers.BatchNormalization()(encoded)
				if i < 3:
					encoded = layers.MaxPooling2D(pool_size=(2, 2), padding='same')(encoded)

		else:
			encoded = layers.Conv2D(filter_nums[0], filter_size, activation='relu', padding='same', input_shape=input_shape)(input_img)
			encoded = layers.BatchNormalization()(encoded)
			encoded =  layers.MaxPooling2D(pool_size=(2, 2), padding='same')(encoded)
			for i in range(1, number_of_layers):
				encoded = layers.Conv2D(filter_nums[i], filter_size, activation='relu', padding='same')(encoded)
				encoded = layers.BatchNormalization()(encoded)
				if i < 2:
					encoded = layers.MaxPooling2D(pool_size=(2, 2), padding='same')(encoded)

	return encoded


def decoder(encoded, number_of_layers, filter_size, filter_nums):
	if number_of_layers < 1:
		print("Need at least 1 layer!")
		return

	decoded = encoded

	if number_of_layers < 3:
		for i in range(number_of_layers-1, -1, -1):
			decoded = layers.Conv2D(filter_nums[i], filter_size, activation='relu', padding='same')(decoded)
			decoded = layers.BatchNormalization()(decoded)
			decoded = layers.UpSampling2D((2, 2))(decoded)

		decoded = layers.Conv2D(1, filter_size, activation='sigmoid', padding='same')(decoded)
		decoded = layers.BatchNormalization()(decoded)

	else:
		if filter_size == 3:
			for i in range(number_of_layers-1, 0, -1):
				decoded = layers.Conv2D(filter_nums[i], filter_size, activation='relu', padding='same')(decoded)
				decoded = layers.BatchNormalization()(decoded)
				if i < 3:
					decoded = layers.UpSampling2D((2, 2))(decoded)

			decoded = layers.Conv2D(filter_nums[i], filter_size, activation='relu')(decoded)
			decoded = layers.BatchNormalization()(decoded)
			decoded = layers.UpSampling2D((2, 2))(decoded)

			decoded = layers.Conv2D(1, filter_size, activation='sigmoid', padding='same')(decoded)
			decoded = layers.BatchNormalization()(decoded)

		else:
			for i in range(number_of_layers-1, -1, -1):
				decoded = layers.Conv2D(filter_nums[i], filter_size, activation='relu', padding='same')(decoded)
				decoded = layers.BatchNormalization()(decoded)
				if i < 2:
					decoded = layers.UpSampling2D((2, 2))(decoded)

			decoded = layers.Conv2D(1, filter_size, activation='sigmoid', padding='same')(decoded)
			decoded = layers.BatchNormalization()(decoded)

	return decoded


def make_autoencoder(rows, cols, number_of_layers, filter_size, filter_nums):
	input_shape = (rows,cols,1)
	input_img = keras.Input(shape=input_shape)

	encoded = encoder(input_img, input_shape, number_of_layers, filter_size, filter_nums)
	decoded = decoder(encoded, number_of_layers, filter_size, filter_nums)

	autoencoder = keras.Model(input_img, decoded)
	autoencoder.compile(loss='mean_squared_error', optimizer=keras.optimizers.RMSprop())

	return autoencoder

def train_autoencoder(autoencoder, train_images, epochs, batch_size):
	train_X, valid_X, train_ground, valid_ground = train_test_split(train_images, train_images, test_size=0.1, random_state=13)

	train_history = autoencoder.fit(train_X, train_ground, batch_size=batch_size, epochs=epochs, verbose=1, validation_data=(valid_X, valid_ground))

	return autoencoder, train_history
