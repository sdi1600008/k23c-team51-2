import numpy as np
import struct
from array import array
from keras.utils import to_categorical

def get_image_data(file_path):
	with open(file_path, 'rb') as file:
		magic_number, num_images, rows, cols = struct.unpack(">IIII", file.read(16))
		image_data = array("B", file.read())

	images = np.ndarray(shape=(num_images, rows, cols), dtype=np.uint8)
	pixel_num = rows*cols
	for i in range(num_images):
		pixels = np.array(image_data[i*pixel_num : (i+1)*pixel_num])
		pixels = pixels.reshape(rows, cols)
		images[i] = pixels
	images = np.reshape(images, (len(images), rows, cols, 1))
	return images, num_images, rows, cols


def get_image_labels(file_path):
	with open(file_path, 'rb') as file:
		magic_number, num_images = struct.unpack(">II", file.read(8))
		labels_data = array("B", file.read())

	labels = np.ndarray(shape=(num_images), dtype=np.uint8)
	for i in range(num_images):
		labels[i] = labels_data[i]

	labels = to_categorical(labels)
	return labels, num_images


def get_positive_number(prompt):
    flag = True
    while flag:
        flag = False
        num = int(input(prompt))
        if num < 0:
            print('Please input a positive number.')
            flag = True
    return num
