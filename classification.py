import numpy as np
import keras
import matplotlib.pyplot as plt
import math
import sys
import getopt
import random
from keras import layers
from keras.models import load_model
from sklearn.model_selection import train_test_split

from src.input import get_image_data, get_image_labels, get_positive_number

#init
args = sys.argv[1:]
optlist, args = getopt.getopt(args,"d:t:", ["dl=", "tl=", "model="])

for opt, arg in optlist:
	if opt == "-d":
		train_image_file = arg
	elif opt == "--dl":
		train_label_file = arg
	elif opt == "-t":
		test_image_file = arg
	elif opt == "--tl":
		test_label_file = arg
	elif opt == "--model":
		autoencoder = arg

# get data & labels
train_images, num_train_images, rows, cols = get_image_data(train_image_file)
train_labels, num_train_labels = get_image_labels(train_label_file)
test_images, num_test_images, rows2, cols2 = get_image_data(test_image_file)
test_labels, num_test_labels = get_image_labels(test_label_file)

if (num_train_images != num_train_labels) or (num_test_images != num_test_labels):
	print("oh no.")
	exit(1)

if (rows != rows2) or (cols != cols2):
	print("oh no.")
	exit(1)

train_images = train_images.astype('float32')/255.0
test_images = test_images.astype('float32')/255.0

split_train_imgs, split_valid_imgs, split_train_labels, split_valid_labels = train_test_split(train_images, train_labels, test_size=0.1, random_state=13)

autoencoder = load_model(autoencoder)

#making the data collections for the final summary
sum_names = ()
sum_loss = []
sum_fc_nodes = []
sum_epochs = []
sum_batch_size = []
count = 0

new_classifier = True
while new_classifier:
	# input
	fc_nodes = get_positive_number('Input Number of Nodes in Fully Connected Layer:')
	epochs_phase_1 = get_positive_number('Input Epochs of phase 1 (no encoder training):')
	epochs_phase_2 = get_positive_number('Input Epochs of phase 2 (full model):')
	batch_size = get_positive_number('Input Batch Size:')

	# Make the classifieer
	encoder = keras.Model(autoencoder.input, autoencoder.layers[(int)((len(autoencoder.layers)-3)/2)+2].output, name="encoder")

	input_shape = (rows,cols,1)
	input_img = keras.Input(input_shape, name="input")
	encoded = encoder(input_img)
	x = layers.Flatten(name="flatten")(encoded)
	x = layers.Dense(fc_nodes, activation='relu', name="FC")(x)
	x = layers.Dropout(0.2, name="dropout")(x)
	x = layers.Dense(10, activation='softmax', name="softmax")(x)

	classifier = keras.Model(input_img,x)
	classifier.compile(loss = 'mean_squared_error', optimizer = keras.optimizers.RMSprop(), metrics = ["accuracy"])

	# Train (epochs_phase_1)
	classifier.layers[1].trainable = False
	classifier.compile(loss = 'mean_squared_error', optimizer = keras.optimizers.RMSprop(), metrics = ["accuracy"])

	history_1 = classifier.fit(split_train_imgs, split_train_labels, batch_size=batch_size, epochs=epochs_phase_1, validation_data=(split_valid_imgs, split_valid_labels))

	# More Train (epochs_phase_2)
	classifier.layers[1].trainable = True
	classifier.compile(loss = 'mean_squared_error', optimizer = keras.optimizers.RMSprop(), metrics = ["accuracy"])

	history_2 = classifier.fit(split_train_imgs, split_train_labels, batch_size=batch_size, epochs=epochs_phase_2, validation_data=(split_valid_imgs, split_valid_labels))

	#data collection for the final summary
	tempname = 'test_'+str(count)
	sum_names = sum_names + (tempname,)
	count = count + 1
	sum_loss.append(history_2.history['loss'][-1])
	sum_fc_nodes.append(fc_nodes)
	sum_epochs.append(epochs_phase_1 + epochs_phase_2)
	sum_batch_size.append(batch_size)

	flag = True
	prompt = '\nSelect an option:\n0:Exit\n1:Repeat the training proccess with new variables\n2:Print training history plots\n3:Evaluate test values\n\n-:'
	while flag:
		num = int(input(prompt))
		if num == 0:
			# exit
			new_classifier = False
			flag = False
		elif num == 1:
			# new classifier
			flag = False
		elif num == 2:
			# show combined history plots

			plt.plot(history_1.history['loss'] + history_2.history['loss'])
			plt.plot(history_1.history['val_loss'] + history_2.history['val_loss'])
			plt.plot(history_1.history['accuracy'] + history_2.history['accuracy'])
			plt.plot(history_1.history['val_accuracy'] + history_2.history['val_accuracy'])
			plt.ylabel('value')
			plt.xlabel('epochs')
			plt.legend(['train loss', 'validation loss', 'accuracy', 'val_accuracy'], loc='upper right')
			plt.show()

			#making data graph
			fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)
			ax1.bar(sum_names, sum_loss, color='red' )
			ax1.set_ylabel('Final Loss')
			ax2.bar(sum_names, sum_fc_nodes, color='orange')
			ax2.set_ylabel('FC nodes')
			ax3.bar(sum_names, sum_epochs )
			ax3.set_ylabel('Epochs')
			ax4.bar(sum_names, sum_batch_size, color='green')
			ax4.set_ylabel('Batch Size')
			plt.show()

		elif num == 3:
			# test data evaluation
			test_scores = classifier.evaluate(test_images, test_labels, verbose=2)
			print("Test loss:", test_scores[0])
			print("Test accuracy:", test_scores[1])

			# show test data classification
			n = 10
			plt.figure(figsize=(20, 4))
			predictions = classifier.predict(test_images)
			predicted_labels = np.argmax(predictions, axis=1)
			true_labels = np.argmax(test_labels, axis=1)
			correct_predicted = np.nonzero(predicted_labels == true_labels)
			wrong_predicted = np.nonzero(predicted_labels != true_labels)

			plt.gray()
			for i in range(n):
				# correct prediction
				r = correct_predicted[0][random.randrange(len(correct_predicted[0]))]
				plt.subplot(2, n, i+1)
				plt.imshow(test_images[r].reshape(28,28), cmap='gray', interpolation='none')
				plt.title("Class {}, Predicted {}".format(true_labels[r], predicted_labels[r]), fontdict={'size': 10})
				plt.axis('off')

				# wrong prediction
				r = wrong_predicted[0][random.randrange(len(wrong_predicted[0]))]
				plt.subplot(2, n, i+n+1)
				plt.imshow(test_images[r].reshape(28,28), cmap='gray', interpolation='none')
				plt.title("Class {}, Predicted {}".format(true_labels[r], predicted_labels[r]), fontdict={'size': 10})
				plt.axis('off')
			plt.show()
		else:
			print('Please input a valid option.')
